source 'https://rubygems.org'


# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.0.0', '>= 5.0.0.1'
# Use Puma as the app server
gem 'puma', '~> 3.0'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
# gem 'jbuilder', '~> 2.5'
# Use Redis adapter to run Action Cable in production
gem 'redis', '~> 3.0'
gem 'redis-rails'
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Use Rack CORS for handling Cross-Origin Resource Sharing (CORS), making cross-origin AJAX possible
gem 'rack-cors'

# MongoID
gem 'mongoid', '~> 6.0.0'

# Sidekiq for jobs
gem 'sidekiq'
gem 'sidekiq-cron'

# Custom Seralize objects
gem 'active_model_serializers'

# Pagination
gem 'kaminari-mongoid'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platform: :mri
end

group :development do
  gem 'listen', '~> 3.0.5'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

group :test do
  # Set of strategies for cleaning your database in Ruby
  gem 'database_cleaner'

  # Fixtures replacement with a straightforward definition syntax
  gem 'factory_girl_rails'

  # Faker data for tests
  gem 'faker'

  # Stubbing and setting expectations on HTTP requests
  gem 'webmock'

  # Testing framework
  gem 'rspec-rails'

  # Matchers and helpers to test for Sidekiq
  gem 'rspec-sidekiq'

  # This gem brings back assigns to your controller tests as well as assert_template to both controller and integration tests.
  gem 'rails-controller-testing'

  # Code coverage analysis tool for Ruby
  gem 'simplecov', :require => false
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]


