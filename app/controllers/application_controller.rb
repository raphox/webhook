class ApplicationController < ActionController::API
  rescue_from Exception, with: :show_errors

  protected
    def show_errors(exception)
      if exception.respond_to?(:record)
        render json: {
          code:    exception.class.to_s.upcase,
          message: exception.message,
          errors:  exception.record.errors.messages
        }
      else
        render json: {
          code:    exception.class.to_s.upcase,
          message: exception.message
        }
      end
    end

    def show_success(message, code, status = 200)
      render json: {
        code:    code.blank? ? "SUCCESS" : code.upcase,
        message: message
      }, status: status
    end
end
