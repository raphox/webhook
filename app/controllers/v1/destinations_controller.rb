module V1

  class DestinationsController < ApplicationController
    before_action :set_destination, only: [:show, :destroy, :message]

    # GET /v1/destinations
    def index
      params[:page] ||= {}

      @destinations = Destination
        .page(params[:page][:number])
        .per(params[:page][:size])

      render json: @destinations
    end

    # GET /v1/destinations/1
    def show
      render json: @destination
    end

    # POST /v1/destinations
    def create
      @destination = Destination.new(destination_params)

      @destination.save!

      render json: @destination, status: :created, location: v1_destination_url(@destination)
    end

    # DELETE /v1/destinations/1
    def destroy
      @destination.destroy

      show_success("Message deleted", nil, :deleted)
    end

    # POST /v1/destinations/1/message
    def message
      attributes = message_params

      ProcessRequestWorker.perform_async({
        destination_id: @destination.id.to_s,
        body:           attributes['body'],
        content_type:   attributes['content-type']
      })

      show_success("Message created", nil, :created)
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_destination
        @destination = Destination.find(params[:id])
      end

      # Only allow a trusted parameter "white list" through.
      def destination_params
        data = params.require(:data).permit(:type, attributes: [ :url ])

        data && data[:type] == 'destinations' ? data[:attributes] : {}
      end

      def message_params
        data = params.require(:data).permit(:type, attributes: [ 'body', 'content-type' ])

        data && data[:type] == 'messages' ? data[:attributes] : {}
      end
  end
end
