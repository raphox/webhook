class Destination
  include Mongoid::Document
  include Mongoid::Timestamps
  field :url, type: String

  #== INDEX =================================================
  index({ url: 1 })

  validates :url, presence: true, uniqueness: true, absence: false
  validates :url, format: { with: URI.regexp(%w(http https)) }, if: Proc.new { |a| a.url.present? }
end
