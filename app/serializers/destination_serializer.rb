class DestinationSerializer < ActiveModel::Serializer
  attributes :id, :url
end
