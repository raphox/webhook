# Process the clear old requests

class ClearOldProcessRequestWorker
  include Sidekiq::Worker

  def perform
    jobs = []

    jobs +=        Sidekiq::Queue.new.select { |job| job.created_at < (Time.now - 24.hours) }
    jobs +=     Sidekiq::RetrySet.new.select { |job| job.created_at < (Time.now - 24.hours) }
    jobs += Sidekiq::ScheduledSet.new.select { |job| job.created_at < (Time.now - 24.hours) }

    jobs.each(&:delete)
  end
end
