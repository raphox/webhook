# Custom exception to identify old jobs on queue
class WaintOldJobException < Exception
end

# Custom exception to identify request error on partiner
class InvalidResponseException < Exception
end

# Process the requests to destination
class ProcessRequestWorker
  include Sidekiq::Worker
  sidekiq_options retry: 3
  sidekiq_retry_in { 6.hours }

  RETRY_ERRORS = [
    EOFError,
    Errno::ECONNRESET,
    Errno::EINVAL,
    Net::HTTPBadResponse,
    Net::HTTPHeaderSyntaxError,
    Net::ProtocolError,
    Timeout::Error,
    InvalidResponseException
  ]

  def perform(data)
    data     = data.is_a?(Hash) ? data.with_indifferent_access : raise(TypeError, 'You must give a Hash object')
    response = nil

    if previous_job(data)
      raise WaintOldJobException, "Waint old proccess"
    end

    # Get the destination
    destination = Destination.find(data['destination_id'].to_s)
    uri         = URI.parse(destination.url)

    Rails.logger.info "[ProcessRequestWorker] Requesting to destination ##{destination.id} #{destination.url}"

    # Setup
    http    = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Post.new(uri.request_uri)

    # Add header
    request.add_field('Content-Type', data['content_type'])

    # Add body
    request.body = data['body']

    # Perform request
    response = http.request(request)

    unless response.kind_of? Net::HTTPSuccess
      raise InvalidResponseException
    end

    Rails.logger.info "[ProcessRequestWorker] Response success."
    Rails.logger.debug response.to_yaml
  rescue WaintOldJobException => e
    # Remove last retry to discard current attempt to retry when previous finished
    Sidekiq::RetrySet.new.each{ |job| job.delete if job.jid == self.jid }

    Rails.logger.info "[ProcessRequestWorker] #{e.message}. The last attempts were discard."

    fail e
  rescue *RETRY_ERRORS => e
    Rails.logger.info "[ProcessRequestWorker] RETRY ERRORS rescue."

    if response
      Rails.logger.debug "[ProcessRequestWorker] Response:"
      Rails.logger.debug response.to_yaml
    end

    fail e
  rescue Exception => e
    Rails.logger.error "[ProcessRequestWorker] Server error on proccess request."
    Rails.logger.error e.message
    Rails.logger.error e.backtrace.join("\n")

    if response
      Rails.logger.info "[ProcessRequestWorker] Response:"
      Rails.logger.info response.to_yaml
    end

    Rails.logger.error "[ProcessRequestWorker] Invalid request data:"
    Rails.logger.error data.to_yaml
  end

  private

    def previous_job(data)
      Sidekiq::Queue.new.detect { |job| self.check_old(data, job) } or
        Sidekiq::RetrySet.new.detect { |job| self.check_old(data, job) } or
        Sidekiq::ScheduledSet.new.detect { |job| self.check_old(data, job) }
    end

    def check_old(data, job)
      job.jid != self.jid and
        job.args[0]['destination_id'].to_s == data['destination_id'].to_s and
        job.created_at < Time.now
    end
end
