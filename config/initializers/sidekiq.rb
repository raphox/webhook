REDIS_URL = ENV["REDIS_URL"].blank? ? 'redis://localhost:6379' : ENV["REDIS_URL"]

###########################################################
# SIDEKIQ SERVER SETTINGS
###########################################################
Sidekiq.default_worker_options = { 'backtrace' => Rails.env.development? }

Sidekiq.configure_server do |config|

  config.redis =  { url: REDIS_URL + '/sidekiq' }

  # config.server_middleware do |chain|
  #   chain.add Sidekiq::Status::ServerMiddleware, expiration: 30.minutes # default
  # end
  # config.client_middleware do |chain|
  #   chain.add Sidekiq::Status::ClientMiddleware
  # end
end


###########################################################
# SIDEKIQ CLIENT SETTINGS
###########################################################
Sidekiq.configure_client do |config|
  config.redis =  { url: REDIS_URL + '/sidekiq' }
end


###########################################################
# SIDEKIQ SCHEDULER SETTINGS
###########################################################
schedule_file = 'config/schedule.yml'

if File.exists?(schedule_file)
  # Remove the comment bellow when you add any scheduled job to the schedule.yml
  Sidekiq::Cron::Job.load_from_hash YAML.load_file(schedule_file) unless Rails.env.test?
end
