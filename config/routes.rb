require 'sidekiq/web'
require 'sidekiq/cron/web'

Rails.application.routes.draw do
  namespace :v1 do
    resources :destinations do
      member do
        post 'message'
      end
    end
  end

  mount Sidekiq::Web => '/sidekiq'
end
