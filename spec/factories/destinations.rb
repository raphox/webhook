FactoryGirl.define do
  factory :destination do
    url { Faker::Internet.url }
  end
end
