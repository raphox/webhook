require 'rails_helper'

RSpec.describe Destination, type: :model do
  it "creates a Destination with valid params" do
    destination = Destination.create(url: "https://hootsuite.com/pt/")

    expect(Destination.last).to eql(destination)
  end
end
