require "rails_helper"

RSpec.describe V1::DestinationsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/v1/destinations").to route_to("v1/destinations#index")
    end

    # it "routes to #new" do
    #   expect(:get => "/v1/destinations/new").to route_to("v1/destinations#new")
    # end

    # it "routes to #show" do
    #   expect(:get => "/v1/destinations/1").to route_to("v1/destinations#show", :id => "1")
    # end

    # it "routes to #edit" do
    #   expect(:get => "/v1/destinations/1/edit").to route_to("v1/destinations#edit", :id => "1")
    # end

    it "routes to #create" do
      expect(:post => "/v1/destinations").to route_to("v1/destinations#create")
    end

    # it "routes to #update via PUT" do
    #   expect(:put => "/v1/destinations/1").to route_to("v1/destinations#update", :id => "1")
    # end

    # it "routes to #update via PATCH" do
    #   expect(:patch => "/v1/destinations/1").to route_to("v1/destinations#update", :id => "1")
    # end

    it "routes to #destroy" do
      expect(:delete => "/v1/destinations/1").to route_to("v1/destinations#destroy", :id => "1")
    end

  end
end
