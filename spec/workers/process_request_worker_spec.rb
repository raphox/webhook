require 'rails_helper'
require 'sidekiq/api'

RSpec.describe ProcessRequestWorker, type: :worker do

  # This should return the minimal set of attributes required to create a valid
  # Destination. As you add validations to Destination, be sure to
  # adjust the attributes here as well.
  let(:valid_attributes) {
    { url: "https://hootsuite.com/pt/" }
  }

  it "delete job if it fails more than 3 times and has lifetime over 24 hours" do
    destination = Destination.create! valid_attributes

    Sidekiq::Testing.inline! do
      5.times do
        ProcessRequestWorker.perform_in(5.seconds, {
          destination_id: destination.id.to_s,
          body: "{\"key\":\"value\",\"anotherKey\": \"anotherValue\"}",
          content_type: "application/json"
        })
      end

      ProcessRequestWorker.new.perform({
        destination_id: destination.id.to_s,
        body: "{\"key\":\"value\",\"anotherKey\": \"anotherValue\"}",
        content_type: "application/json"
      })

      expect(Sidekiq::Queue.new.size).to eq(6)
    end
  end
end
